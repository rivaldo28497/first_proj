import com.sun.org.apache.xml.internal.utils.StringToIntTable;

public class main{
    public static void main(String args[])
    {
        int c = Integer.parseInt(args[0]);
        String str = "default";

        if (args.length > 1) {
            str = args[1];
        }

        switch(str) {
            case "-reverse" :
                reverse(c);
                break;
            case "-forward" :
                forward(c);
                break;
            default:
                forward(c);
                reverse(c);

        }
    }
    public static void forward(int c) {
        for (int i = 0; i < c; i++) {
            for (int i2 = 0; i2 <= i; i2++) {
                System.out.print(i2 + 1 + " ");
            }
            System.out.println();
        }
    }
    public static void reverse(int c) {
        for (int i = c; i >= 0; i--) {
            for (int i2 = 0; i2 <= i; i2++) {
                System.out.print(i2 + 1 + " ");
            }
            System.out.println();
        }
    }

}